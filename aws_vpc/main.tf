############
# Networks #
# VPC      #
############
resource "aws_vpc" "vpc" {
  count                = "${var.create_vpc ? 1 : 0 }"
  cidr_block           = "${var.aws_vpc_network}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    Name = "${var.aws_vpc_name}"
  }
}

############
# Subnets  #
# Private  #
############

resource "aws_subnet" "aws_private_subnets" {
  count      = "${var.create_vpc && length(var.aws_private_subnets) > 0 ? length(var.aws_private_subnets) : 0}"
  vpc_id     = "${aws_vpc.vpc.id}"
  cidr_block = "${var.aws_private_subnets[count.index]}"

  availability_zone = "${var.aws_availability_zones[count.index]}"

  tags = {
    Name = "${var.aws_private_name[count.index]}"
  }

  depends_on = ["aws_vpc.vpc"]
}

###########
# Public  #
###########

resource "aws_subnet" "aws_public_subnets" {
  count                   = "${var.create_vpc && length(var.aws_public_subnets) > 0 ? length(var.aws_public_subnets) : 0}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.aws_public_subnets[count.index]}"
  map_public_ip_on_launch = true

  availability_zone = "${var.aws_availability_zones[count.index]}"

  tags = {
    Name = "${var.aws_public_name[count.index]}"
  }

  depends_on = ["aws_vpc.vpc"]
}

####################
# Internet Gateway #
####################

resource "aws_internet_gateway" "gw" {
  count  = "${var.create_vpc && length(var.aws_public_subnets) > 0 ? 1 : 0 }"
  vpc_id = "${aws_vpc.vpc.id}"

  tags = {
    Name = "igw-${var.aws_vpc_name}"
  }

  depends_on = ["aws_vpc.vpc"]
}

###############
# Nat Gateway #
# EIP         #
###############
resource "aws_eip" "nat_ip" {
  count      = "${var.create_vpc && var.enable_nat_gateway ? 1 : 0 }"
  vpc        = true
  depends_on = ["aws_vpc.vpc", "aws_internet_gateway.gw"]

  tags = {
    Name = "eip-nat-gateway-1"
  }
}

################
# NAT Instance #
################
resource "aws_nat_gateway" "nat_gw" {
  count         = "${var.create_vpc && var.enable_nat_gateway ? 1 : 0 }"
  allocation_id = "${aws_eip.nat_ip.id}"
  subnet_id     = "${element(aws_subnet.aws_public_subnets.*.id, 1)}"

  tags = {
    Name = "NAT Gateway"
  }

  depends_on = ["aws_internet_gateway.gw"]
}

################
# Route tables #
# Private      #
################

resource "aws_default_route_table" "private_route_table" {
  count                  = "${var.create_vpc && length(var.aws_public_subnets) > 0 ? 1 : 0}"
  default_route_table_id = "${aws_vpc.vpc.default_route_table_id}"

  tags {
    Name = "rtb-private"
  }

  depends_on = ["aws_vpc.vpc", "aws_subnet.aws_private_subnets"]
}

resource "aws_route" "private_routes" {
  count                  = "${var.create_vpc && length(var.aws_private_subnets) > 0 && var.enable_nat_gateway ? 1 : 0}"
  route_table_id         = "${aws_default_route_table.private_route_table.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.nat_gw.id}"

  depends_on = ["aws_vpc.vpc", "aws_subnet.aws_private_subnets"]
}

resource "aws_route_table_association" "private_association" {
  count          = "${var.create_vpc && length(var.aws_private_subnets) > 0 ? length(var.aws_private_subnets) : 0}"
  subnet_id      = "${element(aws_subnet.aws_private_subnets.*.id, count.index)}"
  route_table_id = "${aws_default_route_table.private_route_table.id}"

  depends_on = ["aws_vpc.vpc", "aws_subnet.aws_private_subnets"]
}

##########
# Public #
##########

resource "aws_route_table" "public_route_table" {
  count  = "${var.create_vpc && length(var.aws_public_subnets) > 0 ? 1 : 0}"
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "rtb-public"
  }

  depends_on = ["aws_vpc.vpc", "aws_subnet.aws_public_subnets"]
}

resource "aws_route" "public_routes" {
  count                  = "${var.create_vpc && length(var.aws_public_subnets) > 0 ? 1 : 0}"
  route_table_id         = "${aws_route_table.public_route_table.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw.id}"

  depends_on = ["aws_vpc.vpc", "aws_subnet.aws_public_subnets"]
}

resource "aws_route_table_association" "public_association" {
  count          = "${var.create_vpc && length(var.aws_public_subnets) > 0 ? length(var.aws_public_subnets) : 0}"
  subnet_id      = "${element(aws_subnet.aws_public_subnets.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_route_table.id}"

  depends_on = ["aws_vpc.vpc", "aws_subnet.aws_public_subnets"]
}

###################
# Security Groups #
# Default         #
###################

resource "aws_default_security_group" "default" {
  count = "${var.create_vpc ? 1 : 0}"

  #  name        = "base"
  vpc_id = "${aws_vpc.vpc.id}"

  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["${var.aws_vpc_network}"]
    self        = true
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    self        = true
  }

  tags {
    Name        = "sg-default"
    Description = "default VPC Security Group"
  }

  depends_on = ["aws_vpc.vpc"]
}

################
# Network ACLs #
################

resource "aws_default_network_acl" "default" {
  count                  = "${var.create_vpc ? 1 : 0}"
  default_network_acl_id = "${aws_vpc.vpc.default_network_acl_id}"

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    Name = "Default ACL"
  }
}

#######
# Key #
#######
resource "aws_key_pair" "admin" {
  count      = "${var.create_vpc ? 1 : 0 }"
  key_name   = "terraform-admin-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCqwTjrOy/wo9GRd2AQznF0Zx1l99cdJIReJRvBlBWlnvHN1QJNNt0LAW5W+xVlZr4RQsaRLuIDhkorz9519Ye1CvcUaQY7hKz+7HC8SERmG8NclOkHC6z1wyk0edJgdoI5XdGQPYYZAQf3/NqbsPw6loKLw+HqRfA+wwJybd5p5wNBddFo0E+5jq32rdLfHrHdzDD5G93XYEQz0vGEgPTydCTQQyX/0BwbO8dZoONaloARKzE40rNDzF/x367DabYAwiq/4rKSCE1Uw/FQMrkOoq1UWPlXLKgAuHQxVoMV6f9r6+TFVGtmXlvBnOkO0jWU3xp9F4gFXi9lyOqM4VlF3ikIgqlmZ175ijLYDNt7S1DMSrk8FTUTk/HI8KDQImelQ23kcJIa/1DLl9ewn5ac4TGqSSThamM3H3lQucHALGqIIRx1DFyQWhmr/BBAmwHtY4BX+ExFpgfcCAJHPPVwvx5GNnDqlapQmfR+jFSyTN7L2vToalbsBFpcTZAUYNfmGlvGH64xrzLTX4ozAVpV0obermLp6Qgdi95asVyTc4MlyLhYK+0BWZWaLSoSzfnSv4d94BIOqpJ8cBpHeSt3N5SjgdgW6rlGRxXRgHUlKOjqdMz1AudVgmjaoyw0sA5jB8PB2ooml4nV0GAdQe7kWzWVUx4lXlo3b3zWyDU6/w== avgur@avgur.od.ua"
}
