output "aws_vpc_id" {
  value = "${element(split(",", join(",", aws_vpc.vpc.*.id)), 0)}"
}

output "aws_vpc_network" {
  value = "${element(split(",", join(",", aws_vpc.vpc.*.cidr_block)), 0)}"
}

output "aws_default_sg_id" {
  value = "${element(split(",", join(",", aws_default_security_group.default.*.id)), 0)}"
}

output "aws_private_subnet_ids" {
  value = "${element(split(",", join(",", aws_subnet.aws_private_subnets.*.id)), 0)}"
}

output "aws_public_subnet_ids" {
  value = "${element(split(",", join(",", aws_subnet.aws_public_subnets.*.id)), 0)}"
}

output "aws_public_route_table_ids" {
  value = "${aws_route_table.public_route_table.*.id}"
}

output "aws_private_route_table_ids" {
  value = "${aws_default_route_table.private_route_table.*.id}"
}

output "aws_public_subnets" {
  value = "${aws_subnet.aws_public_subnets.*.cidr_block}"
}

output "aws_private_subnets" {
  value = "${aws_subnet.aws_private_subnets.*.cidr_block}"
}

output "aws_security_group_base_id" {
  value = "${element(split(",", join(",", aws_security_group.security_group_base.*.id)), 0)}"
}
