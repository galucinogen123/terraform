###################
# Security Group  #
# Base            #
###################
resource "aws_security_group" "security_group_base" {
  count = "${var.create_vpc && var.create_sg_base ? 1 : 0 }"

  name        = "base"
  description = "Base Security Group"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["188.187.124.51/32"]
    description = "office"
  }

  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["40.91.197.226/32"]
    description = "devops-1"
  }

  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["212.29.231.234/32"]
    description = "office-israel"
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["188.187.124.51/32"]
    description = "office"
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["40.91.197.226/32"]
    description = "devops-1"
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["212.29.231.234/32"]
    description = "office-israel"
  }

  tags {
    Name        = "sg-base"
    Description = "Base Security Group"
  }
}
