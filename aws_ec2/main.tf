#################
# EC2 Instances #
# Test          #
#################
resource "aws_instance" "instance" {
  count         = "${var.create_instance ? var.number_of_instances : 0 }"
  ami           = "${var.aws_ami}"
  instance_type = "${var.aws_instance_type}"

  subnet_id = "${element(var.aws_subnet_ids, 0)}"

  key_name = "${var.admin_key_name}"

  vpc_security_group_ids = ["${var.aws_security_groups}"]

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "20"
    delete_on_termination = true
  }

  tags = "${var.aws_tags}"
}

resource "aws_eip" "instance_elastic_ip" {
  count = "${var.create_instance && var.create_eip ? 1 : 0 }"

  vpc = true

  instance                  = "${aws_instance.instance.id}"
  associate_with_private_ip = "${aws_instance.instance.private_ip}"

  tags = {
    Name = "eip-instance-1"
  }
}
