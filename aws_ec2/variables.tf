variable "create_instance" {
  description = "The "
}

variable "create_eip" {
  description = "The "
}

variable "aws_ami" {
  description = "ID of AMI to use for the instance"
}

variable "aws_instance_type" {
  description = "The "
}

variable "aws_subnet_ids" {
  description = "The "
  type        = "list"
}

variable "aws_tags" {
  description = "The "
  type        = "map"
}

variable "aws_security_groups" {
  description = "The "
  type        = "list"
}

variable "number_of_instances" {
  description = "The "
}

variable "admin_key_name" {
  description = "The admin_key_name"
  default     = "terraform-admin-key"
}

variable "private_route_table_ids" {
  description = "The private_route_table_ids"
  type        = "list"
  default     = [""]
}

variable "public_route_table_ids" {
  description = "The public_route_table_ids"
  type        = "list"
  default     = [""]
}

variable "aws_check" {
  description = "The "
  default     = true
}

variable "depends_on" {
  default = []

  type = "list"
}

variable dependency {
  default = ""
}
