output "private_ip" {
  value = "${element(split(",", join(",", aws_instance.instance.*.private_ip)), 0)}"
}

#output "security_groups" {
#  value = "${aws_security_group.security_group.*.id}"
#}

